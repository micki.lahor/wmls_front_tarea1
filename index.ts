import axios from 'axios'
import { Producto } from './interface/producto.interface'
export type List = Producto[]

async function obtenerProductos() {

  const { data } = await axios.get<List>('https://fakestoreapi.com/products')
  console.log("🚀 ~ Respuesta Productos: ", data)
  
  const productosOrdenadosPrecio = data.sort((a, b) => b.price - a.price);
  console.log("🚀 ~ Productos ordenados por precio DESC: ", productosOrdenadosPrecio);

  const productosFiltrados = data.filter(producto => producto.rating.rate > 2.9);
  console.log("🚀 ~ Productos Filtrados > 2.9", productosFiltrados);
}

obtenerProductos()